package lodash

import "sync"

type Procedure[TCtx ProcedureContext] struct {
	Ctx    TCtx
	Err    error
	Events []interface{}
}

type ProcedureContext interface {
	sync.Locker
}

func NewProcedure[TCtx ProcedureContext](ctx TCtx) *Procedure[TCtx] {
	return &Procedure[TCtx]{
		Ctx:    ctx,
		Events: make([]interface{}, 0),
	}
}

func (p *Procedure[TCtx]) NextGroup(fns ...NextFunc[TCtx]) *Procedure[TCtx] {
	p.Ctx.Lock()
	defer p.Ctx.Unlock()
	if p.Err != nil {
		return p
	}
	for _, fn := range fns {
		err := fn(p.Ctx)
		if err != nil {
			p.Err = err
			return p
		}
	}
	return p
}

func (p *Procedure[TCtx]) NextGroupWithEvents(fns ...NextWithEventFunc[TCtx]) *Procedure[TCtx] {
	p.Ctx.Lock()
	defer p.Ctx.Unlock()
	if p.Err != nil {
		return p
	}
	for _, fn := range fns {
		err := fn(p.Ctx, p.addEvent)
		if err != nil {
			p.Err = err
			return p
		}
	}
	return p
}

func (p *Procedure[TCtx]) Async(fns ...NextFunc[TCtx]) *Procedure[TCtx] {
	if p.Err != nil {
		return p
	}
	for _, fn := range fns {
		go fn(p.Ctx)
	}
	return p
}

func (p *Procedure[TCtx]) Next(fn NextFunc[TCtx]) *Procedure[TCtx] {
	p.Ctx.Lock()
	defer p.Ctx.Unlock()
	if p.Err != nil {
		return p
	}
	err := fn(p.Ctx)
	if err != nil {
		p.Err = err
		return p
	}
	return p
}

func (p *Procedure[TCtx]) NextWithEvents(fn NextWithEventFunc[TCtx]) *Procedure[TCtx] {
	p.Ctx.Lock()
	defer p.Ctx.Unlock()
	if p.Err != nil {
		return p
	}
	err := fn(p.Ctx, p.addEvent)
	if err != nil {
		p.Err = err
		return p
	}
	return p
}

func (p *Procedure[TCtx]) addEvent(event interface{}) {
	p.Events = append(p.Events, event)
}
