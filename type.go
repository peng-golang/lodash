package lodash

type AddEventFunc func(interface{})
type NextFunc[TCtx ProcedureContext] func(ctx TCtx) error
type NextWithEventFunc[TCtx ProcedureContext] func(ctx TCtx, eventFunc AddEventFunc) error
