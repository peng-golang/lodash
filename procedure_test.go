package lodash

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"sync"
	"testing"
	"time"
)

type TestCtx struct {
	count int
	*sync.Mutex
}

func NewTestCtx() *TestCtx {
	return &TestCtx{
		Mutex: &sync.Mutex{},
	}
}

func TestProcedureError(t *testing.T) {
	p := NewProcedure(NewTestCtx()).
		Next(func(ctx *TestCtx) error {
			ctx.count++
			return nil
		})

	assert.Equal(t, 1, p.Ctx.count)
}

func TestProcedureNext(t *testing.T) {
	p := NewProcedure(NewTestCtx()).
		Next(func(ctx *TestCtx) error {
			ctx.count++
			return nil
		}).
		Next(func(ctx *TestCtx) error {
			ctx.count++
			return nil
		})

	assert.Equal(t, 2, p.Ctx.count)
}

func TestProcedureErr(t *testing.T) {
	p := NewProcedure(NewTestCtx()).
		Next(func(ctx *TestCtx) error {
			return errors.New("error")
		})
	assert.Error(t, p.Err)
}

type TestEvent struct {
	Count int
}

func TestGetSpecificEvent(t *testing.T) {
	p := NewProcedure(NewTestCtx()).
		NextWithEvents(func(ctx *TestCtx, addEvent AddEventFunc) error {
			ctx.count++
			addEvent(TestEvent{Count: 1})
			addEvent(&TestEvent{Count: 2})
			return nil
		})
	assert.Nil(t, p.Err)
	events := GetEvents[TestEvent](p)
	assert.Equal(t, 1, len(events))

}

func TestConditionalNext(t *testing.T) {
	ctx := NewTestCtx()
	wg := &sync.WaitGroup{}
	wg.Add(2)
	go func(ctx *TestCtx) {
		t.Logf("start runtime")
		NewProcedure(ctx).NextGroup(
			func(ctx *TestCtx) error {
				t.Logf("%d sleep 1s", time.Now().UnixMilli())
				time.Sleep(1 * time.Second)
				return nil
			}, func(ctx *TestCtx) error {
				t.Logf("%d add in runtime", time.Now().UnixMilli())
				ctx.count++
				return nil
			},
		)
		wg.Done()
	}(ctx)

	go func() {
		t.Logf("start main")
		NewProcedure(ctx).Next(func(ctx *TestCtx) error {
			t.Logf("start add")
			ctx.count++
			return nil
		})
		wg.Done()
	}()
	wg.Wait()

	t.Logf("end count %d", ctx.count)
}

func TestConcurrentRun(t *testing.T) {
	ctx := NewTestCtx()

	for i := 0; i < 1000; i++ {
		go NewProcedure(ctx).Next(func(ctx *TestCtx) error {
			ctx.count++
			return nil
		})
	}
	time.Sleep(time.Second)
	t.Logf("count: %d", ctx.count)
}
