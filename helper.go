package lodash

func GetEvents[TEvent any, TCtx ProcedureContext](p *Procedure[TCtx]) []TEvent {
	p.Ctx.Lock()
	defer p.Ctx.Unlock()
	events := make([]TEvent, 0)
	for _, event := range p.Events {
		if specificEvent, ok := event.(TEvent); ok {
			events = append(events, specificEvent)
		}
	}
	return events
}
